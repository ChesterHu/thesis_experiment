
def merge_nonzeros(fname1, fname2):
    lines = []
    with open(fname1) as f1, open(fname2) as f2:
        for line1, line2 in zip(f1, f2):
            rho, _, rtime, std = line1.split(',')
            _, nonzero, _, _ = line2.split(',')
            lines.append(','.join([rho, nonzero, rtime, std]))
    
    with open(fname1, 'w') as f:
        f.writelines(lines)

merge_nonzeros('out/rand_coord_rho_comlj', 'out/ista_rho_comlj')
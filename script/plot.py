from scipy.interpolate import make_interp_spline, BSpline

import matplotlib.pyplot as plt
import numpy as np
import sys

def smooth_line(x_values, y_values, scale, num_pts = 20):
    if scale == 'log':
        x_new = np.logspace(np.log10(min(x_values)), np.log10(max(x_values)), num_pts)
    else:
        x_new = np.linspace(min(x_values), max(x_values), num_pts)
    
    spl = make_interp_spline(x_values, y_values, k=3)
    y_new = spl(x_new)
    return x_new, y_new


def set_tick_size():
    ax = plt.gca()
    ax.tick_params(axis = 'both', which = 'major', labelsize = 24)
    ax.tick_params(axis = 'both', which = 'minor', labelsize = 16)


def plot(exp_type, fname, fontsize, method_name, linestyle = '-', linewidth = 3, marker = 'o', markersize = 14):
    set_tick_size()
    xaxis_values, rtimes, deviations = [], [], []

    try:
        with open(fname) as f:
            for line in f:
                # parse file depends on experiment type
                if exp_type == 'alpha':
                    xaxis_value, rtime, std = [float(val) for val in line.split(',')]
                elif exp_type == 'rho':
                    xaxis_value, _, rtime, std = [float(val) for val in line.split(',')]
                else:
                    _, xaxis_value, rtime, std = [float(val) for val in line.split(',')]
                
                xaxis_values.append(xaxis_value)
                rtimes.append(rtime)
                deviations.append(std)
    except:
        print(f"Can't open file: {fname}")
        return
    
    if exp_type == 'alpha':
        scale = 'lin'
    else:
        scale = 'log'

    if exp_type == 'nonzeros':
        xaxis_values = xaxis_values[::-1]
        rtimes = rtimes[::-1]
        deviations = deviations[::-1]

    x_values, rtimes = smooth_line(xaxis_values, rtimes, scale)
    _, deviations = smooth_line(xaxis_values, deviations, scale)

    x_values, rtimes, deviations = np.array(x_values), np.array(rtimes)*1000, np.array(deviations)*1000
    
    plt.plot(x_values, rtimes, label=method_name, linewidth=linewidth, linestyle=linestyle, marker = marker, markersize = markersize)
    plt.fill_between(x_values, rtimes-deviations, rtimes+deviations, alpha = 0.2)


if __name__ == "__main__":
    if len(sys.argv) != 3:
        print('-'*62)
        print('WRONG INPUT ARGUMENTS!')
        print('usage: python3 script/plot.py [experiment type] [graph names]')
        print('experiment types: alpha, rho, nonzeros')
        print('graph names: jh, comlj, youtube, ...')
        print('example: python3 script/plot.py alpha youtube')
        print('-'*62)
        sys.exit()
    elif sys.argv[1] not in ['alpha', 'rho', 'nonzeros']:
        print(f'Unknown experiment type: {sys.argv[1]}')
        sys.exit()

    font_size = 30
    line_width = 5
    fig_size = (14, 10)

    out_path = 'out'
    fig_path = 'fig'
    exp_type = sys.argv[1]
    suf_name = sys.argv[2]

    if exp_type == 'alpha':
        out_file = 'alpha'
    else:
        out_file = 'rho'  # rho and nonzeros experiment results are stored in the same file

    plt.figure(figsize = fig_size)

    plot(exp_type, f'{out_path}/ista_{out_file}_{suf_name}', font_size, 'ISTA', linestyle='-', marker = 'o')
    plot(exp_type, f'{out_path}/fista_{out_file}_{suf_name}', font_size, 'FISTA', linestyle='-', marker = 'v')
    plot(exp_type, f'{out_path}/linear_couple_{out_file}_{suf_name}', font_size, 'Linear couple', linestyle='-', marker = '*')
    plot(exp_type, f'{out_path}/rand_coord_{out_file}_{suf_name}', font_size, 'Random coordinate descent', linestyle='-.', marker = 's')
    plot(exp_type, f'{out_path}/fista_const_{out_file}_{suf_name}', font_size, 'FISTA (const stepsize)', linestyle='-.', marker = 'p')
    plot(exp_type, f'{out_path}/linear_couple_restart_{out_file}_{suf_name}', font_size, 'Linear couple (with restart)', linestyle='-.', marker = 'D')

    if exp_type == 'alpha':
        xlabel = r'$\alpha$'
    elif exp_type == 'rho':
        xlabel = r'$\rho$'
    else:
        xlabel = 'Number of nonzero vertices'

    ylabel = 'Runtime (ms)'
    plt.xlabel(xlabel, fontsize = font_size)
    plt.ylabel(ylabel, fontsize = font_size)

    if exp_type == 'nonzeros':
        legend_loc = 'upper left'
    else:
        legend_loc = 'upper right'

    plt.legend(fontsize = font_size * .7, loc = legend_loc)
    plt.grid(linestyle = 'dashed')

    if exp_type == 'rho' or exp_type == 'nonzeros':
        plt.xscale('log')

    plt.savefig(f'{fig_path}/runtime_{exp_type}_{suf_name}')
    print(f'figure store in {fig_path}/runtime_{exp_type}_{suf_name}')
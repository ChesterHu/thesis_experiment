import sys

def merge_line(line_rho, line_nonzeros):
    rho, rtime, std = line_rho.split(',')
    nonzeros, _, _ = line_nonzeros.split(',')
    return ','.join([rho, nonzeros, rtime, std])

def merge_file(file_rho, file_nonzeros, file_out):
    lines = []
    with open(file_rho) as frho, open(file_nonzeros) as fnonzeros:
        for line_rho, line_nonzeros in zip(frho, fnonzeros):
            line = merge_line(line_rho, line_nonzeros)
            lines.append(line)

    with open(file_out, 'w') as fout:
        fout.writelines(lines)

if __name__ == "__main__":
    file_rho = sys.argv[1]
    file_nonzeros = sys.argv[2]
    file_out = sys.argv[3]

    merge_file(file_rho, file_nonzeros, file_out)
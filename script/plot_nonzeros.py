import matplotlib.pyplot as plt
import numpy as np
import sys

def set_tick_size():
    ax = plt.gca()
    ax.tick_params(axis = 'both', which = 'major', labelsize = 24)
    ax.tick_params(axis = 'both', which = 'minor', labelsize = 16)

def plot_nonzeros(fname):
    fontsize = 30
    method_names = ['ISTA', 'FISTA', 'Linear couple', 'FISTA (const stepsize)', 'Linear couple (with restart)']
    linestyles = ['-', '-', '-', '-.', '-.']
    marks = ['o', 'v', '*', 'p', 'D']
    optimal_num_nonzeros = 0
    with open(fname) as f:
        i = 0
        for line in f:
            if i == 3:
                plt.plot([], [])
            nonzeros = [float(v) for v in line.split(',')]
            optimal_num_nonzeros = nonzeros[-1]
            plt.plot(nonzeros[:75], label=method_names[i], linestyle = linestyles[i], linewidth=3, marker = marks[i], markersize = 14, markevery=8)
            i += 1

    plt.axhline(y=optimal_num_nonzeros, color='k', linestyle='-.', linewidth=5, label='Optimal solution')
    plt.legend(fontsize = 30*.7, loc='lower right')
    plt.grid(linestyle='dashed')
    plt.xlabel(r'Iteration $k$', fontsize=fontsize)
    plt.ylabel(r'Number of nonzero vertices', fontsize=fontsize)

if __name__ == "__main__":
    file_suffix = sys.argv[1]
    fname = f'out/nonzeros_{file_suffix}'
    plt.figure(figsize = (14, 10))
    set_tick_size()
    plot_nonzeros(fname)
    plt.savefig(f'fig/nonzeros_{file_suffix}')

import matplotlib.pyplot as plt
import networkx as nx
import numpy as np
import sys

import localgraphclustering as lgc

GRAPH_NAME = 'ppi_mips'
GRAPH_TYPE = 'graphml'

fname = f'data/{GRAPH_NAME}.{GRAPH_TYPE}'
graph = lgc.GraphLocal(fname, GRAPH_TYPE)
node2id = [0 for _ in range(graph._num_vertices)]

cnt = 0
# copy and paste the node id into the list

with open(f'out/clusters_ppi') as f:
    for line in f:
        cnt += 1
        cluster = [int(v) for v in line.split(',')]
        for node in cluster:
            node2id[node] = cnt

coords = np.loadtxt(f'data/{GRAPH_NAME}.xy')

'''
cluster = []
drawing = graph.draw(coords, nodesize = 5, edgealpha = 0.5, nodecolor = 'k', figsize = (45, 33))
drawing.nodecolor(cluster, c = 'r')
'''
# single usroads figure size (35, 30)
# jh figure (30, 20)
# usroads figure size (28, 24)

graph.draw_groups(coords, node2id, nodesize=200, edgealpha=0.25, edgecolor='k', figsize=(30, 33))
plt.title(r'Protein-protein Interactions', fontsize = 80)
plt.subplots_adjust(left=0.001, right=0.999, top=0.80, bottom=0.01)

#plt.show()
plt.savefig('fig/clustering_ppi')

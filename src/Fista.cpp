#include "../include/Fista.h"

///////////////////
// Public functions
///////////////////

Fista::Fista(double alpha, double epsilon, double rho, Graph& graph)
    : PageRankSolver(alpha, epsilon, rho, graph)
    , is_constant_beta(false)
{}

void Fista::set_beta_constant(bool flag) {
    is_constant_beta = flag;
}

////////////////////
// Private functions
////////////////////

bool Fista::is_nonzero(long int node) const {
    double thd = rho*alpha*graph->get_d_sqrt(node);

    if (fabs(x[node]) > FLOAT_PRECISION || fabs(y[node]) > FLOAT_PRECISION) {
        return true;
    } else if (gradient[node] <= -thd) {
        return true;
    } else if (gradient[node] >= thd) {
        return true;
    }
    return false;
}

void Fista::update(int num_iter) {
    double beta = get_beta();
    unordered_set<long int> nodes_touched;
    vector<pair<long int, double>> node_to_dy;
    
    for (long int node : nonzero_set) {
        double dx = get_dx(node);
        x[node] += dx;
        double dy = x[node]+beta*dx-y[node];
        y[node] += dy;
        node_to_dy.push_back(make_pair(node, dy));
    }

    update_gradient(node_to_dy, nodes_touched);
    update_nonzeros(nodes_touched);
}

void Fista::update_gradient(vector<pair<long int, double>>& node_to_dy, unordered_set<long int>& nodes_touched) {
    for (auto iter : node_to_dy) {
        long int node = iter.first;
        double dy = iter.second;
        gradient[node] += dy-.5*(1-alpha)*dy;
        nodes_touched.insert(node);

        for (long int neighbor : *(graph->get_neighbors(node))) {
            gradient[neighbor] -= .5*(1-alpha)*graph->get_dn_sqrt(node)*graph->get_dn_sqrt(neighbor)*dy;
            nodes_touched.insert(neighbor);
        }
    }
}

void Fista::update_nonzeros(unordered_set<long int>& nodes_touched) {
    for (long int node : nodes_touched) {
        if (is_nonzero(node)) {
            nonzero_set.insert(node);
        } else {
            nonzero_set.erase(node);
        }
    }
}

double Fista::get_dx(long int node) const {
    double dx;
    double thd = rho*alpha*graph->get_d_sqrt(node);

    if (y[node]-gradient[node] >= thd) {
        dx = y[node]-gradient[node]-thd-x[node];
    } else if (y[node]-gradient[node] <= -thd) {
        dx = y[node]-gradient[node]+thd-x[node];
    } else {
        dx = -x[node];
    }
    return dx;
}

double Fista::get_beta() {
    if (is_constant_beta) {
        return (1-sqrt(alpha))/(1+sqrt(alpha));
    }
    double prev_t = t;
    t = (1+sqrt(1+4*t*t))/2;
    return (prev_t - 1)/t;
}

////////////////////////////
// Private virtual functions
////////////////////////////

void Fista::init_x() {
    t = 1;
    x = y = vector<double>(graph->num_vertices(), 0);
}

bool Fista::is_converged(int num_iter) {
    if (num_iter < 2) return false;
    double l2 = 0;
    for (long int node : nonzero_set) {
        l2 += (x[node]-y[node])*(x[node]-y[node]);
    }
    l2 = sqrt(l2);
    return l2 < epsilon;
}
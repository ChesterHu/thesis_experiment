#include "../include/Test.h"

// Seed node to test
static long int SEED_NODE;
static bool EVALUATE_FVALUE;
////////////////////////////
// Test function definitions
////////////////////////////

void print_hline() {
    cout << "--------------------------------\n";
}

void print_settings() {
    cout << "\n************* TEST SETTINGS *************\n";
    cout << "alpha                  : " << ALPHA << '\n';
    cout << "rho                    : " << RHO << '\n';
    cout << "epsilon                : " << EPSILON << '\n';
    cout << "max iter               : " << MAX_ITER << '\n';
    cout << "seed vertex            : " << SEED_NODE << '\n';
    cout << "evaluate function value: " << EVALUATE_FVALUE << '\n';
}

void test_pagerank(Graph &graph) {
    cout << "Choose a seed vertex (from " << 0 << " to "  << graph.num_vertices() << "):\n";
    cin >> SEED_NODE;
    cout << "Evaluate function values? (this will take very long time for large graphs) [y/n]\n";
    EVALUATE_FVALUE = get_y_or_n();
    print_settings();

    cout << "\n************** TEST PAGERANK RESULTS **************\n";
    
    cout << "\nTesting ISTA...\n";
    Ista ista(ALPHA, EPSILON, RHO, graph);
    test_pagerank_solver(ista, graph, EVALUATE_FVALUE);
    print_hline();

    cout << "\nTesting FISTA...\n";
    Fista fista(ALPHA, EPSILON, RHO, graph);
    test_pagerank_solver(fista, graph, EVALUATE_FVALUE);
    print_hline();

    cout << "\nTesting Linear couple...\n";
    MirrorDescent linear_couple(ALPHA, EPSILON, RHO, graph);
    test_pagerank_solver(linear_couple, graph, EVALUATE_FVALUE);
    print_hline();

    cout << "\nTesting Random coordinate descent...\n";
    RandomCoordinateDescent rand_coord(ALPHA, EPSILON, RHO, graph);
    test_pagerank_solver(rand_coord, graph, EVALUATE_FVALUE);
    print_hline();

    cout << "\nTesting FISTA with const stepsize...\n";
    fista.set_beta_constant(true);
    test_pagerank_solver(fista, graph, EVALUATE_FVALUE);
    print_hline();

    cout << "\nTesting linear couple with restart...\n";
    linear_couple.set_restart_flag(true);
    test_pagerank_solver(linear_couple, graph, EVALUATE_FVALUE);

    cout << "\n******************* All TEST DONE *******************\n";
    cout << "\nContinue test pagerank algorithms? [y/n]\n";
    if (get_y_or_n()) {
        return test_pagerank(graph);
    }
}

void test_clustering(Graph& graph) {
    cout << "Start testing clustering\n";
    cout << "Choose a seed vertex (from " << 0 << " to "  << graph.num_vertices() << "):\n";
    cin >> SEED_NODE;
    double alpha, rho;
    cout << "alpha:\n";
    cin >> alpha;
    cout << "rho:\n";
    cin >> rho;
    Ista ista(alpha, EPSILON, rho, graph);
    test_sweep(ista);
}

void test_all(Graph &graph) {
    test_pagerank(graph);

    cout << "Test sweep clustering? [y/n]\n";
    if (get_y_or_n()) {
        test_clustering(graph);
    }
}

void test_fvalue(Graph& graph, Objective& obj) {
    double value_all = 1;
    vector<double> x(graph.num_vertices(), value_all);
    double fvalue = obj.get_fvalue(x, graph);
    cout << "x all equal to " << value_all << " fvalue: " << fvalue << endl;
}

void test_gradient(Graph& graph, Objective& obj) {
    double value_all = 1;
    vector<double> x(graph.num_vertices(), value_all);
    vector<double> gradient(obj.get_gradient(x, graph));

    double sum_gradient = 0;
    for (double g : gradient)
        sum_gradient += g;
    cout << "x all equal to " << value_all << " sum of gradient: " << sum_gradient << endl;
}

void test_proximal_gradient(Graph& graph, Objective& obj) {
    int max_iter = 20;
    vector<double> x(graph.num_vertices(), 0);
    ProximalGradient prox_method(graph, obj);

    prox_method.minimize(x, max_iter);

    double fvalue = obj.get_fvalue(x, graph);
    long int num_nonzeros = obj.get_num_nonzeros(x);
    cout << "proximal gradient get fvalue: " << fvalue << '\n';
    cout << "number of nonzeros          : " << num_nonzeros << '\n';
}

void test_appr(double alpha, double epsilon, double rho, Graph& graph, Objective& obj) {
    vector<long int> seed_nodes = {0};
    Appr appr(alpha, epsilon, rho, graph);

    PageRankResult result = appr.minimize(seed_nodes, MAX_ITER);

    double fvalue = obj.get_fvalue(result.x, graph);
    long int num_nonzeros = obj.get_num_nonzeros(result.x);
    cout << "appr get fvalue   : " << fvalue << '\n';
    cout << "number of nonzeros: " << num_nonzeros << '\n';
}

void test_pagerank_solver(PageRankSolver& solver, Graph& graph, bool evaluate_fvalue) {
    vector<long int> seed_nodes = {SEED_NODE};
    Objective obj(ALPHA, RHO, seed_nodes);
    vector<double> distribution = vector<double>(graph.num_vertices(), 0);
    distribution[SEED_NODE] = 1;
    PageRankResult result = solver.minimize(distribution, MAX_ITER);
    // PageRankResult result = solver.minimize(seed_nodes, MAX_ITER);
    if (evaluate_fvalue) {
        double fvalue = obj.get_fvalue(result.x, graph);
        cout << "get fvalue        : " << fvalue << endl;
    }
    cout << "number of nonzeros: " << result.num_nonzeros << endl;
    cout << "runtime           : " << result.runtime << " sec" << endl;
}

void test_sweep(PageRankSolver& solver) {
    vector<long int> seed_nodes = {SEED_NODE};
    solver.minimize(seed_nodes, MAX_ITER);
    vector<long int> cluster = solver.sweep();
    sort(cluster.begin(), cluster.end());

    print_hline();
    cout << "get cluster of size: " << cluster.size() << '\n';
    cout << "nodes in cluster:\n["; 
    for (size_t i = 0; i < cluster.size(); ++i) {
        cout << cluster[i];
        if (i < cluster.size() - 1) cout << ", ";
    }
    cout << "]\n";
    print_hline();
}
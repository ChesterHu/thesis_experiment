#include "../include/Ista.h"

///////////////////
// Public functions
///////////////////

Ista::Ista(double alpha, double epsilon, double rho, Graph& graph)
    : PageRankSolver(alpha, epsilon, rho, graph)
{}

////////////////////
// Private functions
////////////////////

void Ista::update(int num_iter) {
    unordered_set<long int> nodes_to_add;

    for (long int node : nonzero_set) {
        double dx = -gradient[node]-rho*alpha*graph->get_d_sqrt(node);
        x[node] += dx;
        gradient[node] += dx-.5*(1-alpha)*dx;

        for (long int neighbor : *(graph->get_neighbors(node))) {
            gradient[neighbor] -= .5*(1-alpha)*graph->get_dn_sqrt(node)*graph->get_dn_sqrt(neighbor)*dx;
            if (!nonzero_set.count(neighbor) && x[neighbor]-gradient[neighbor] >= rho*alpha*graph->get_d_sqrt(neighbor))
                nodes_to_add.insert(neighbor);
        }
    }

    for (long int node : nodes_to_add)
        nonzero_set.insert(node);
}
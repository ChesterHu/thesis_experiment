#include <iostream>
#include <vector>
#include <string.h>

#include "../include/Ista.h"
#include "../include/MirrorDescent.h"
#include "../include/Fista.h"
#include "../include/RandomCoordinateDescent.h"
#include "../include/Test.h"
#include "../include/Utilities.h"

using namespace std;

int main(int argc, char* argv[]) {
    if (argc < 2) {
        cout << "Usage: " << argv[0] << " [test/runtime/cluster/accuracy]\n";
        return 0;
    }

    cout << "Choose one of following graphs (type the ID):\n";
    for (int i = 0; i < GRAPHS.size(); ++i) {
        cout << i << " " + GRAPHS[i] << endl;
    }
    int graph_id;
    cin >> graph_id;

    // set graph name and type
    string graph_name = GRAPH_NAMES[graph_id];
    string graph_type = GRAPH_TYPES[graph_id];
    string output_file_suffix = OUTPUT_FILE_SUFFIXES[graph_id];

    // load the graph
    cout << "\nLoading graph: " << GRAPHS[graph_id] << '\n';
    Graph graph;
    graph.read_graph(graph_name, graph_type);

    if (strcmp(argv[1], "test") == 0) {
        test_all(graph);  // test
    } else if (strcmp(argv[1], "runtime") == 0) {
        run_experiment(graph_id, graph);  // run experiments
    } else if (strcmp(argv[1], "cluster") == 0) {
        find_clusters(graph_id, graph);
    } else {
        cout << "Unknown arg: " << argv[1] << '\n';
    }
}

#include "../include/ProximalGradient.h"

///////////////////
// Public functions
///////////////////

ProximalGradient::ProximalGradient(Graph& graph, Objective& obj) {
    this->graph = graph;
    this->obj = obj;
}

vector<double> ProximalGradient::minimize(vector<double>& x, int max_iter) const {
    while (max_iter-- > 0) {
        vector<double> gradient = this->obj.get_gradient(x, this->graph);
        this->proximal_step(x, gradient);
    }
    return x;
}

////////////////////
// Private functions
////////////////////

void ProximalGradient::proximal_step(vector<double>& x, vector<double>& gradient) const {
    double alpha = this->obj.get_alpha();
    double rho = this->obj.get_rho();

    for (long int i = 0; i < x.size(); ++i) {
        if (x[i] - gradient[i] >= rho*alpha*this->graph.get_d_sqrt(i)) {
            x[i] = x[i] - gradient[i] - rho*alpha*this->graph.get_d_sqrt(i);
        } else if (x[i] - gradient[i] <= -rho*alpha*this->graph.get_d_sqrt(i)) {
            x[i] = x[i] - gradient[i] + rho*alpha*this->graph.get_d_sqrt(i);
        } else {
            x[i] = 0;
        }
    }
}
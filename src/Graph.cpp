#include "../include/Graph.h"

///////////////////
// Public functions
///////////////////

long int Graph::num_vertices() const {
    return neighbors.size();
}

long int Graph::find_node_max_deg() const {
    long int node = 0;
    double max_deg_sqrt = 0;
    for (long int v = 0; v < this->num_vertices(); ++v) {
        if (this->get_d_sqrt(v) > max_deg_sqrt) {
            max_deg_sqrt = this->get_d_sqrt(v);
            node = v;
        }
    }
    return node;
}

bool Graph::is_connect(long int i, long int j) const {
    return this->neighbors[i].count(j);
}

double Graph::get_d_sqrt(size_t idx) const {
    return this->d_sqrt[idx];
}

double Graph::get_dn_sqrt(size_t idx) const {
    return this->dn_sqrt[idx];
}

const unordered_set<long int>* Graph::get_neighbors(long int idx) const {
    return &neighbors[idx];
}

void Graph::read_graph(const string fname, const string graph_type) {
    if (graph_type == GRAPHML) {
        read_graphml(fname);
    } else if(graph_type == SNAP_TXT) {
        read_snap_txt(fname);
    } else {
        cout << "Unkown graph type: " << graph_type << '\n';
        exit(-1);
    }
    cout << "Graph loaded with number of vertices: " << this->num_vertices() << '\n';
}

void Graph::read_graphml(string fname) {
    FILE *fstream = fopen(&fname[0], "r");

    if (fstream != NULL) {
        igraph_t graph;
        igraph_read_graph_graphml(&graph, fstream, 0);
        this->init(&graph);
        fclose(fstream);
    } else {
        cout << "Can't open file: " << fname << endl;
        exit(-1);
    }
}

void Graph::read_snap_txt(string fname) {
    FILE *fstream = fopen(&fname[0], "r");

    if (fstream != NULL) {
        igraph_t graph;
        igraph_read_graph_edgelist(&graph, fstream, 0, 1);
        this->init(&graph);
        fclose(fstream);
    } else {
        cout << "Can't open file: " << fname << endl;
        exit(-1);
    }
}

////////////////////
// Private functions
////////////////////

void Graph::init(igraph_t *graph) {
    this->init_neighbors(graph);
    this->init_d_sqrt();
    this->init_dn_sqrt();
}

void Graph::init_neighbors(igraph_t *graph) {
    igraph_vector_t neighbors;
    igraph_vector_init(&neighbors, igraph_vcount(graph));
    this->neighbors = vector<unordered_set<long int> >(igraph_vcount(graph));

    for (igraph_integer_t i = 0; i<igraph_vcount(graph); ++i) {
        igraph_neighbors(graph, &neighbors, i, IGRAPH_ALL);
        for (long int j = 0; j < igraph_vector_size(&neighbors); ++j)
            this->neighbors[i].insert(VECTOR(neighbors)[j]);
    }

    igraph_vector_destroy(&neighbors);
}

void Graph::init_d_sqrt() {
    this->d_sqrt = vector<double>(this->num_vertices());
    for (long int i = 0; i < this->num_vertices(); ++i)
        this->d_sqrt[i] = sqrt(this->neighbors[i].size());
}

void Graph::init_dn_sqrt() {
    this->dn_sqrt = vector<double>(this->num_vertices());
    for (long int i = 0; i < this->num_vertices(); ++i)
        this->dn_sqrt[i] = 1/this->d_sqrt[i];
}
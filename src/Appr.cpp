#include "../include/Appr.h"

///////////////////
// Public functions
///////////////////

Appr::Appr(double alpha, double epsilon, double rho, Graph& graph)
    : PageRankSolver(alpha, epsilon, rho, graph)
{}

////////////////////
// Private functions
////////////////////

void Appr::update(int num_iter) {
    long int node = *nonzero_set.begin();
    nonzero_set.erase(node);

    x[node] -= gradient[node];
    for (long int neighbor : *(graph->get_neighbors(node))) {
        gradient[neighbor] += .5*(1-alpha)*graph->get_dn_sqrt(node)*graph->get_dn_sqrt(neighbor)*gradient[node];
        if (is_nonzero(neighbor))
            nonzero_set.insert(neighbor);
        else
            nonzero_set.erase(neighbor);
    }
    gradient[node] *= .5*(1-alpha);

    if (is_nonzero(node))
        nonzero_set.insert(node);
}

bool Appr::is_nonzero(long int node) const {
    return gradient[node] < -alpha*rho*graph->get_d_sqrt(node);
}

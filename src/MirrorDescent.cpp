#include "../include/MirrorDescent.h"

///////////////////
// Public functions
///////////////////

MirrorDescent::MirrorDescent(double alpha, double epsilon, double rho, Graph& graph)
    : PageRankSolver(alpha, epsilon, rho, graph)
    , restart_flag(false)
{}

void MirrorDescent::set_restart_flag(bool flag) {
    restart_flag = flag;
}

////////////////////
// Private functions
////////////////////

bool MirrorDescent::is_nonzero(long int node) const {
    double thd = rho*alpha*graph->get_d_sqrt(node);

    if (fabs(x[node]) > FLOAT_PRECISION || fabs(y[node]) > FLOAT_PRECISION || fabs(z[node]) > FLOAT_PRECISION) {
        return true;
    } else if (gradient[node] <= -thd) {
        return true;
    } else if (gradient[node] >= thd) {
        return true;
    }
    return false;
}

void MirrorDescent::update(int num_iter) {
    if (restart_flag) {
        if (iter_cnt > sqrt(1/alpha))
            iter_cnt = 0;
        num_iter = iter_cnt++;
    }
    double next_tau = get_next_tau(num_iter);
    double gamma = 1/get_tau(num_iter);
    unordered_set<long int> nodes_touched;
    vector<pair<long int, double> > node_to_dx;
    
    for (long int node : nonzero_set) {
        double dy = get_dy(node);
        double dz = get_dz(node, gamma);
        y[node] += dy;
        z[node] += dz;

        double dx = get_dx(y[node], z[node], x[node], next_tau);
        x[node] += dx;  // x+dx=x_{num_iter+2}
        node_to_dx.push_back(make_pair(node, dx));
    }

    update_gradient(node_to_dx, nodes_touched);
    update_nonzeros(nodes_touched);
}

void MirrorDescent::update_gradient(vector<pair<long int, double> >& node_to_dx, unordered_set<long int>& nodes_touched) {
    for (auto iter : node_to_dx) {
        long int node = iter.first;
        double dx = iter.second;
        gradient[node] += dx-.5*(1-alpha)*dx;
        nodes_touched.insert(node);
        
        for (long int neighbor : *(graph->get_neighbors(node))) {
            gradient[neighbor] -= .5*(1-alpha)*graph->get_dn_sqrt(node)*graph->get_dn_sqrt(neighbor)*dx;
            nodes_touched.insert(neighbor);
        }
    }
}

void MirrorDescent::update_nonzeros(unordered_set<long int>& nodes_touched) {
    for (long int node : nodes_touched) {
        if (is_nonzero(node)) {
            nonzero_set.insert(node);
        } else {
            nonzero_set.erase(node);
        }
    }
}

double MirrorDescent::get_dx(double y, double z, double prev_x, double next_tau) const {
    return next_tau*z+(1-next_tau)*y-prev_x;
}

double MirrorDescent::get_dy(long int node) const {
    double dy;
    double thd = rho*alpha*graph->get_d_sqrt(node);
    if (x[node]-gradient[node] >= thd) {
        dy = x[node]-gradient[node]-thd-y[node];
    } else {
        dy = -y[node];
    }
    return dy;
}

double MirrorDescent::get_dz(long int node, double gamma) const {
    double dz;
    double thd = rho*alpha*graph->get_d_sqrt(node);
    if (z[node]-gamma*gradient[node] >= gamma*thd) {
        dz = -gamma*(gradient[node]+thd);
    } else {
        dz = -z[node];
    }
    return dz;
}

double MirrorDescent::get_next_tau(int num_iter) const {
    if (restart_flag && (num_iter+1) > sqrt(1/alpha))
        num_iter = -1;
    return 2.0/(num_iter+3);
}

double MirrorDescent::get_tau(int num_iter) const {
    return 2.0/(num_iter+2);
}

////////////////////////////
// Private virtual functions
////////////////////////////

void MirrorDescent::init_x() {
    iter_cnt = 0;
    x = y = z = vector<double>(graph->num_vertices(), 0);
}

bool MirrorDescent::is_converged(int num_iter) {
    if (num_iter < 2 || (restart_flag && iter_cnt < 2)) return false;
    double l2 = 0;
    for (long int node : nonzero_set) {
        l2 += (x[node]-y[node])*(x[node]-y[node]);
    }
    l2 = sqrt(l2);
    return l2 < epsilon;
}
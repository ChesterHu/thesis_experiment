#include "../include/Objective.h"

///////////////////
// Public functions
///////////////////

Objective::Objective() {}

Objective::Objective(double alpha, double rho, vector<long int>& seed_nodes) {
    this->alpha = alpha;
    this->rho = rho;
    this->s = seed_nodes;
}

double Objective::get_alpha() const {
    return this->alpha;
}

double Objective::get_rho() const {
    return this->rho;
}

double Objective::get_fvalue(vector<double>& x, const Graph& graph) const {
    double fvalue = this->rho*this->alpha*this->get_l1(x, graph);
    for (long int i = 0; i < x.size(); ++i)
        for (long int j = 0; j < x.size(); ++j)
            fvalue += .5*x[i]*x[j]*this->get_Qij(graph, i, j);

    for (long int i : this->s)
        fvalue -= this->alpha*graph.get_dn_sqrt(i)*x[i]/this->s.size();
    return fvalue;
}

long int Objective::get_num_nonzeros(vector<double>& x) const {
    long int cnt = 0;
    for (double val : x) {
        cnt += fabs(val) > FLOAT_PRECISION;
    }
    return cnt;
}

vector<double> Objective::get_gradient(vector<double>& x, const Graph& graph) const {
    vector<double> gradient(x.size(), 0);
    for (long int i = 0; i < x.size(); ++i)
        for (long int j = 0; j < x.size(); ++j)
            gradient[i] += this->get_Qij(graph, i, j)*x[j];
    
    for (long int i : this->s)
        gradient[i] -= this->alpha*graph.get_dn_sqrt(i)/this->s.size();
    return gradient;
}

////////////////////
// Private functions
////////////////////

double Objective::get_l1(vector<double>& x, const Graph& graph) const {
    double l1 = 0;
    for (long int i = 0; i < x.size(); ++i)
        l1 += fabs(x[i]) * graph.get_d_sqrt(i);

    return l1;
}

double Objective::get_Qij(const Graph& graph, long int i, long int j) const {
    double Qij = 0;
    if (i == j)
        Qij += .5*(1+this->alpha);

    if (graph.is_connect(i, j))
        Qij -= .5*(1-this->alpha)*graph.get_dn_sqrt(i)*graph.get_dn_sqrt(j);
    return Qij;
}
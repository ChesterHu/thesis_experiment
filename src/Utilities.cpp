#include "../include/Utilities.h"

static double PERCENTAGE_TO_TEST;
static int START_POINT;

bool is_overlap(unordered_set<long int>& visited, vector<long int> cluster);

// get usr input 
bool get_y_or_n() {
    string usr_input;
    cin >> usr_input;
    return usr_input == "y" ? true : false;
}

// This will create the file named fname, if file 
// already exists, the content of the file will be cleared.
void create_file(string fname) {
    ofstream file;
    file.open(fname, std::ios_base::out | std::ios_base::trunc);
    file.close();
}

vector<double> linspace(double start, double end, int num_pts) {
    vector<double> res;
    if (num_pts > 0) {
        double stepsize = num_pts == 1 ? 0 : (end-start)/(num_pts-1);
        res.push_back(start);
        while (--num_pts > 0) res.push_back(start += stepsize);
    }
    return res;
}

vector<double> logspace(double start, double end, int num_pts) {
    vector<double> res;
    double linstart = log(start);
    double linend = log(end);
    for (double v : linspace(linstart, linend, num_pts)) res.push_back(exp(v));
    return res;
}

long int get_stepsize(long int total, double percentage) {
    if (percentage == 1) {
        return 1;
    }
    return 1 / percentage;
}

double get_avg(vector<double>& nums) {
    double sum = 0;
    for (double num : nums) sum += num;
    return sum/nums.size();
}

double get_std(vector<double>& nums) {
    double sum = 0;
    double avg = get_avg(nums);
    for (double num : nums) sum += (num-avg)*(num-avg);
    return sqrt(sum/(nums.size() - 1));
}

void run_experiment(int graph_id, Graph& graph) {
    cout << "\n************* EXPERIMENT SETTINGS *************\n";

    cout << "\nChoose one of following experiment: (type the ID):\n";
    cout << "0 alpha\n1 rho and nonzeros\n2 multiple clusters total time\n3 nonzeros in iterations\n";
    int experiment_id;
    cin >> experiment_id;
    
    if (experiment_id < 3) {
        cout << "Type in the percentage of " << GRAPHS[graph_id] << " to test (from 0 to 1):\n";
        cin >> PERCENTAGE_TO_TEST;

        if (experiment_id < 2) {
            cout << "\nResume last experiment and skip steps? [y/n]:\n";
            if (get_y_or_n()) {
                cout << "Input number of loops to skip (from 0 to " << NUM_POINTS_IN_PLOT <<"):\n";
                cin >> START_POINT;
            } else {
                START_POINT = 0;
            }
        }
    }
    
    if (experiment_id == 0) {
        run_alpha_experiment(graph_id, graph);
    } else if (experiment_id == 1) {
        run_rho_experiment(graph_id, graph);
    } else if (experiment_id == 2) {
        run_multiple_clusters_experiment(graph_id, graph);
    } else if (experiment_id == 3) {
        run_nonzeros_experiment(graph_id, graph);
    } else {
        cout << "Unknown experiment\n";
        exit(-1);
    }
}

PageRankSolver* select_algorithm(Graph& graph, int& alg_id) {
    cout << "Choose one of following algorithm (type the ID):\n";
    for (int i = 0; i < ALGORITHM_NAMES.size(); ++i)
        cout << i << " "+ALGORITHM_NAMES[i] << '\n';
    cin >> alg_id;

    if (ALGORITHM_NAMES[alg_id] == ISTA) {
        static Ista ista(ALPHA, EPSILON, RHO, graph);
        return &ista;
    } else if (ALGORITHM_NAMES[alg_id] == FISTA) {
        static Fista fista(ALPHA, EPSILON, RHO, graph);
        return &fista;
    } else if (ALGORITHM_NAMES[alg_id] == FISTA_CONST) {
        static Fista fista(ALPHA, EPSILON, RHO, graph);
        fista.set_beta_constant(true);
        return &fista;
    } else if (ALGORITHM_NAMES[alg_id] == LINEAR_COUPLE) {
        static MirrorDescent linear_couple(ALPHA, EPSILON, RHO, graph);
        return &linear_couple;
    } else if (ALGORITHM_NAMES[alg_id] == LINEAR_COUPLE_RESTART) {
        static MirrorDescent linear_couple(ALPHA, EPSILON, RHO, graph);
        return &linear_couple;
    } else if (ALGORITHM_NAMES[alg_id] == RAND_COORD_DESCENT){
        static RandomCoordinateDescent rand_coord(ALPHA, EPSILON, RHO, graph);
        return &rand_coord;
    } else {
        exit(-1);
    }
}

void run_alpha_experiment(int graph_id, Graph& graph) {
    int alg_id;
    PageRankSolver *solver = select_algorithm(graph, alg_id);
    string output_file_name = OUTPUT_FILE_PATH+ALGORITHM_NAMES[alg_id]+"_alpha"+OUTPUT_FILE_SUFFIXES[graph_id];

    cout << "\n************** START EXPERIMENT **************\n";
    alpha_experiment(graph, output_file_name, *solver);
    cout << "\n*************** EXPERIMENT END ***************\n";
    cout << "\noutput stored in file: " << output_file_name << '\n';
}

void run_rho_experiment(int graph_id, Graph& graph) {
    int alg_id;
    PageRankSolver *solver = select_algorithm(graph, alg_id);
    string output_file_name = OUTPUT_FILE_PATH+ALGORITHM_NAMES[alg_id]+"_rho"+OUTPUT_FILE_SUFFIXES[graph_id];
    
    cout << "\n************** START EXPERIMENT **************\n";
    rho_experiment(graph, output_file_name, *solver);
    cout << "\n*************** EXPERIMENT END ***************\n";
    cout << "\noutput stored in file: " << output_file_name << '\n';
}

void run_multiple_clusters_experiment(int graph_id, Graph& graph) {
    int alg_id;
    PageRankSolver *solver = select_algorithm(graph, alg_id);
    string output_file_name = OUTPUT_FILE_PATH+ALGORITHM_NAMES[alg_id]+"_clusters"+OUTPUT_FILE_SUFFIXES[graph_id];
    
    cout << "\n************** START EXPERIMENT **************\n";
    multiple_clusters_experiment(graph, output_file_name, *solver);
    cout << "\n*************** EXPERIMENT END ***************\n";
    cout << "\noutput stored in file: " << output_file_name << '\n';
}

void run_nonzeros_experiment(int graph_id, Graph& graph) {
    long int seed_node;
    double alpha, rho;
    cout << "alpha:\n";
    cin >> alpha;
    cout << "rho:\n";
    cin >> rho;
    cout << "seed vertex:\n";
    cin >> seed_node;

    Ista ista(alpha, EPSILON, rho, graph);
    Fista fista(alpha, EPSILON, rho, graph);
    MirrorDescent linear_couple(alpha, EPSILON, rho, graph);

    string output_file_name = OUTPUT_FILE_PATH+"nonzeros"+OUTPUT_FILE_SUFFIXES[graph_id];

    create_file(output_file_name);
    nonzeros_experiment(seed_node, output_file_name, ista);
    nonzeros_experiment(seed_node, output_file_name, fista);
    nonzeros_experiment(seed_node, output_file_name, linear_couple);
    fista.set_beta_constant(true);
    nonzeros_experiment(seed_node, output_file_name, fista);
    linear_couple.set_restart_flag(true);
    nonzeros_experiment(seed_node, output_file_name, linear_couple);
    cout << "results stored in file: " << output_file_name << '\n';
}


void alpha_experiment(Graph& graph, string fname, PageRankSolver& solver) {
    // if resume the experiment, don't clean the file
    if (START_POINT == 0) {
        create_file(fname);
    }

    long int stepsize = get_stepsize(graph.num_vertices(), PERCENTAGE_TO_TEST);
    vector<double> alphas = linspace(ALPHA_START, ALPHA_END, NUM_POINTS_IN_PLOT);
    
    for (double alpha : alphas) {
        if (START_POINT-- > 0) continue;
        vector<double> rtimes;
        solver.set_alpha(alpha);

        for (long int seed_node = 0; seed_node < graph.num_vertices(); seed_node += stepsize) {
            vector<long int> seed_nodes = {seed_node};

            if (seed_node % (stepsize * PRINT_FREQENCY) == 0) {
                cout << "computing alpha: " << alpha << " for vertex: " << seed_node << endl;
            }

            for (long int t = 0; t < NUM_TRIES_TO_AVG; ++t) {
                PageRankResult result = solver.minimize(seed_nodes, MAX_ITER);
                rtimes.push_back(result.runtime);
            }
        }

        ofstream file;
        file.open(fname, ios_base::out | ios_base::app);
        file << alpha << ',' << get_avg(rtimes) << ',' << get_std(rtimes) << '\n';
        file.close();
    }
}

void rho_experiment(Graph& graph, string fname, PageRankSolver& solver) {
    // if resume the experiment, don't clean the file
    if (START_POINT == 0) {
        create_file(fname);
    }

    long int stepsize = get_stepsize(graph.num_vertices(), PERCENTAGE_TO_TEST);
    vector<double> rhos = logspace(RHO_START, RHO_END, NUM_POINTS_IN_PLOT);
    
    for (double rho : rhos) {
        if (START_POINT-- > 0) continue;
        solver.set_rho(rho);
        vector<double> rtimes;
        double num_nonzeros = 0;
        long int num_seed_vertices = graph.num_vertices() / stepsize;

        for (long int seed_node = 0; seed_node < graph.num_vertices(); seed_node += stepsize) {
            vector<long int> seed_nodes = {seed_node};

            if (seed_node % (stepsize * PRINT_FREQENCY) == 0) {
                cout << "computing rho: " << rho << " for vertex: " << seed_node << endl;
            }

            for (long int t = 0; t < NUM_TRIES_TO_AVG; ++t) {
                PageRankResult result = solver.minimize(seed_nodes, MAX_ITER);
                rtimes.push_back(result.runtime);
                num_nonzeros += result.num_nonzeros / (double)num_seed_vertices;
            }
        }

        ofstream file;
        file.open(fname, ios_base::out | ios_base::app);
        file << rho << ',' << num_nonzeros << ',' << get_avg(rtimes) << ',' << get_std(rtimes) << '\n';
        file.close();
    }
}

void nonzeros_experiment(long int seed_node, string fname, PageRankSolver& solver) {
    vector<long int> seed_nodes = {seed_node};
    PageRankResult result = solver.minimize(seed_nodes, 200, true);

    ofstream file;
    file.open(fname, ios_base::out | ios_base::app);
    
    for (int i = 0; i < result.num_nonzeros_vec.size(); ++i) {
        file << result.num_nonzeros_vec[i];
        if (i < result.num_nonzeros_vec.size() - 1) file << ',';
    }

    file << '\n';
    file.close();
}

void multiple_clusters_experiment(Graph& graph, string fname, PageRankSolver& solver) {
    solver.set_alpha(MULT_CLUSTERS_ALPHA);
    solver.set_rho(MULT_CLUSTERS_RHO);
    long int num_clusters = 0;
    long int stepsize = get_stepsize(graph.num_vertices(), PERCENTAGE_TO_TEST);
    double num_seed_vertices = graph.num_vertices() / stepsize;
    double total_time = 0;
    long int cluster_size = 0;
    unordered_set<long int> visited;

    for (long int seed_node = 0; seed_node < graph.num_vertices(); seed_node += stepsize) {
        vector<long int> seed_nodes = {seed_node};
        PageRankResult result = solver.minimize(seed_nodes, MAX_ITER);
        total_time += result.runtime;
        vector<long int> cluster = solver.sweep();
        if (!is_overlap(visited, cluster)) {
            num_clusters += 1;
            cluster_size += cluster.size();
            for (long int node : cluster) visited.insert(node);
        }
        
        if (seed_node % (stepsize * PRINT_FREQENCY) == 0) {
            cout << "seed_node: " << seed_node << '\n';
            cout << "number of clusters: " << num_clusters << '\n';
            cout << "current total time: " << total_time << '\n';
            cout << "total cluster size: " << cluster_size << '\n';
        }
    }

    ofstream file;
    file.open(fname, ios_base::out);
    file << MULT_CLUSTERS_ALPHA << ',' << MULT_CLUSTERS_RHO << ',' << total_time << ',' << cluster_size << ',' << num_clusters << '\n';
    file.close();
}

void find_clusters(int graph_id, Graph& graph) {
    unordered_set<long int> visited;

    string output_file_name = OUTPUT_FILE_PATH+"clusters"+OUTPUT_FILE_SUFFIXES[graph_id];
    create_file(output_file_name);
    double alpha, rho;
    cout << "Choose alpha:\n";
    cin >> alpha;
    cout << "Choose rho:\n";
    cin >> rho;

    int alg_id, min_cluster_size, num_clusters;
    PageRankSolver* solver = select_algorithm(graph, alg_id);
    solver->set_alpha(alpha);
    solver->set_rho(rho);

    cout << "Choose min cluster size:\n";
    cin >> min_cluster_size;
    cout << "Choose number of clusters:\n";
    cin >> num_clusters;

    for (long int v = 0; v < graph.num_vertices(); v++) {
        if (visited.count(v)) continue;
        vector<long int> seed_nodes = {v};
        solver->minimize(seed_nodes, MAX_ITER);
        vector<long int> cluster = solver->sweep();

        if (cluster.size() < min_cluster_size || is_overlap(visited, cluster)) continue;
        cout << "find cluster!" << endl;
        ofstream file;
        file.open(output_file_name, ios_base::out | ios_base::app);
        for (long int i = 0; i < cluster.size(); ++i) {
            file << cluster[i];
            if (i < cluster.size() - 1) file << ',';
            visited.insert(cluster[i]);
        }
        file << '\n';
        file.close();

        if (--num_clusters <= 0) return;
    }
}

bool is_overlap(unordered_set<long int>& visited, vector<long int> cluster) {
    for (long int node : cluster) {
        if (visited.count(node)) return true;
    }
    return false;
}

#include "../include/RandomCoordinateDescent.h"

///////////////////
// Public functions
///////////////////

RandomCoordinateDescent::RandomCoordinateDescent(double alpha, double epsilon, double rho, Graph& graph)
    : PageRankSolver(alpha, epsilon, rho, graph)
{}

////////////////////
// Private functions
////////////////////
long int RandomCoordinateDescent::sample() {
    return candidate_nodes[rand() % candidate_nodes.size()];
}

////////////////////////////
// Private virtual functions
////////////////////////////

void RandomCoordinateDescent::update(int num_iter) {
    long int node = sample();
    double l_const = (1 + alpha) / 2;
    double dx = -gradient[node] - rho*alpha*graph->get_d_sqrt(node);
    dx /= l_const;
    x[node] += dx;
    gradient[node] += dx-.5*(1-alpha)*dx;

    for (long int neighbor : *(graph->get_neighbors(node))) {
        gradient[neighbor] -= .5*(1-alpha)*graph->get_dn_sqrt(node)*graph->get_dn_sqrt(neighbor)*dx;
        if (!nonzero_set.count(neighbor) && x[neighbor]*l_const-gradient[neighbor] >= rho*alpha*graph->get_d_sqrt(neighbor)) {
            nonzero_set.insert(neighbor);
            candidate_nodes.push_back(neighbor);
        }
    }
}

void RandomCoordinateDescent::init_x() {
    x = vector<double>(graph->num_vertices(), 0);
    candidate_nodes = vector<long int>();
    for (long int node: nonzero_set)
        candidate_nodes.push_back(node);
}

bool RandomCoordinateDescent::is_converged(int num_iter) {
    if (num_iter % candidate_nodes.size() == 0) {
        for (long int node : nonzero_set)
            if (fabs(gradient[node]) > (1+epsilon)*rho*alpha*graph->get_d_sqrt(node))
                return false;
        return true;
    }
    return false;
}
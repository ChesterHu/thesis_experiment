#include "../include/PageRankSolver.h"

///////////////////
// Public functions
///////////////////

PageRankSolver::PageRankSolver(double alpha, double epsilon, double rho, Graph& graph)
    : alpha(alpha)
    , epsilon(epsilon)
    , rho(rho)
{
    this->graph = &graph;
}

void PageRankSolver::set_alpha(double alpha) {
    this->alpha = alpha;
}

void PageRankSolver::set_epsilon(double epsilon) {
    this->epsilon = epsilon;
}

void PageRankSolver::set_rho(double rho) {
    this->rho = rho;
}

PageRankResult PageRankSolver::minimize(vector<long int>& seed_nodes, int max_iter, bool run_to_max_iter) {
    init_nonzero_set(seed_nodes);
    init_gradient();
    init_x();

    return minimize_helper(max_iter, run_to_max_iter);
}

PageRankResult PageRankSolver::minimize(vector<double>& distribution, int max_iter, bool run_to_max_iter) {
    nonzero_set.clear();
    gradient = vector<double>(graph->num_vertices(), 0);
    for (long int node = 0; node < distribution.size(); ++node) {
        if (fabs(distribution[node]) > FLOAT_PRECISION) {
            nonzero_set.insert(node);
            gradient[node] = -alpha*graph->get_dn_sqrt(node) * distribution[node];
        }
    }
    init_x();

    return minimize_helper(max_iter, run_to_max_iter);
}

PageRankResult PageRankSolver::minimize_helper(int max_iter, bool run_to_max_iter) {
    PageRankResult result;
    result.num_nonzeros = 0;
    result.num_nonzeros_vec.push_back(0);
    result.runtime = double(clock());
    for (int num_iter = 0; num_iter < max_iter; ++num_iter) {
        update(num_iter);
        if (!run_to_max_iter && is_converged(num_iter)) break;
        result.num_nonzeros_vec.push_back(nonzero_set.size());
    }

    result.runtime = (double(clock()) - result.runtime) / CLOCKS_PER_SEC;
    result.num_nonzeros = nonzero_set.size();
    result.x = x;
    return result;
}

vector<long int> PageRankSolver::sweep() const {
    long int sweep_cut = 0;
    long int num_edges_leave = 0;
    double current_cluster_degree = 0;
    double min_conductance = 100;
    unordered_set<long int> current_cluster;
    
    vector<pair<double, long int> > sweep_vec = this->get_sweep_vec();

    for (long int idx = 0; idx < sweep_vec.size(); ++idx) {
        double prob_divid_degree = sweep_vec[idx].first;
        long int node = sweep_vec[idx].second;
        
        current_cluster.insert(node);
        current_cluster_degree += graph->get_neighbors(node)->size();

        for (long int neighbor : *graph->get_neighbors(node)) {
            if (current_cluster.count(neighbor)) {
                num_edges_leave -= 1;
            } else {
                num_edges_leave += 1;
            }
        }

        if (num_edges_leave / current_cluster_degree < min_conductance) {
            min_conductance = num_edges_leave / current_cluster_degree;
            sweep_cut = idx;
        }
    }

    vector<long int> sweep_cluster;
    for (long int i = 0; i < sweep_vec.size() && i <= sweep_cut; ++i) {
        long int node = sweep_vec[i].second;
        sweep_cluster.push_back(node);
    }

    return sweep_cluster;
}

////////////////////
// Private functions
////////////////////

bool PageRankSolver::is_converged(int num_iter) {
    for (long int node : nonzero_set)
        if (fabs(gradient[node]) > (1+epsilon)*rho*alpha*graph->get_d_sqrt(node))
            return false;
    return true;
}

void PageRankSolver::init_nonzero_set(vector<long int>& seed_nodes) {
    nonzero_set.clear();
    for (long int node: seed_nodes)
        nonzero_set.insert(node);
}

vector<pair<double, long int> > PageRankSolver::get_sweep_vec() const {
    vector<pair<double, long int> > sweep_vec;

    for (long int node = 0; node < graph->num_vertices(); ++node) {
        if (x[node] != 0 && graph->get_d_sqrt(node) != 0) {
            sweep_vec.push_back({x[node] / graph->get_neighbors(node)->size(), node});
        }
    }

    sort(sweep_vec.begin(), sweep_vec.end());
    reverse(sweep_vec.begin(), sweep_vec.end());

    return sweep_vec;
}

////////////////////////////
// Private virtual functions
////////////////////////////

void PageRankSolver::init_gradient() {
    gradient = vector<double>(graph->num_vertices(), 0);
    for (long int node : nonzero_set)
        gradient[node] = -alpha*graph->get_dn_sqrt(node)/nonzero_set.size();
}

void PageRankSolver::init_x() {
    x = vector<double>(graph->num_vertices(), 0);
}
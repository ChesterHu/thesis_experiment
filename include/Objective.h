#ifndef OBJECTIVE_INCLUDED
#define OBJECTIVE_INCLUDED

#include <cmath>
#include <vector>

#include "Graph.h"

using namespace std;

// This class is used to compare the result of different methods. It's developed 
// mainly for correctness, so the time/space complexity is not optimized.

class Objective {
    public:
        Objective();
        Objective(double alpha, double rho, vector<long int>& seed_nodes);
        
        double get_alpha() const;
        double get_rho() const;
        double get_fvalue(vector<double>& x, const Graph& graph) const;
        long int get_num_nonzeros(vector<double>& x) const;
        vector<double> get_gradient(vector<double>& x, const Graph& graph) const;
    
    private:
        double get_l1(vector<double>& x, const Graph& graph) const;
        double get_Qij(const Graph& graph, long int i, long int j) const;

    private:
        double alpha;
        double rho;
        vector<long int> s;
};

#endif  // OBJECTIVE_INCLUDED
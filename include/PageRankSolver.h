#ifndef PAGE_RANK_SOLVER_INCLUDED
#define PAGE_RANK_SOLVER_INCLUDED

#include <algorithm>
#include <ctime>
#include <cmath>
#include <unordered_set>
#include <vector>

#include "Graph.h"

using namespace std;

struct PageRankResult;

// This class is the interface of all methods
class PageRankSolver {
    public:
        PageRankSolver(double alpha, double epsilon, double rho, Graph& graph);
        void set_alpha(double alpha);
        void set_epsilon(double epsilon);
        void set_rho(double rho);

        PageRankResult minimize(vector<long int>& seed_nodes, int max_iter, bool run_to_max_iter = false);
        PageRankResult minimize(vector<double>& distribution, int max_iter, bool run_to_max_iter = false);
        vector<long int> sweep() const;

    private:
        void init_nonzero_set(vector<long int>& seed_nodes);
        PageRankResult minimize_helper(int max_iter, bool run_to_max_iter);
        vector<pair<double, long int> > get_sweep_vec() const;
        
        virtual bool is_converged(int num_iter);
        virtual void init_gradient();
        virtual void init_x();
        virtual void update(int num_iter) = 0;

    protected:
        double alpha;
        double epsilon;
        double rho;

        unordered_set<long int> nonzero_set;
        vector<double> gradient;
        vector<double> x;
        
        Graph *graph;
};

// This structure contains result of page rank solver
struct PageRankResult {
    PageRankResult() {
        runtime = num_nonzeros = 0;
    }

    double runtime;
    long int num_nonzeros;
    vector<double> x;
    vector<double> num_nonzeros_vec;
};

#endif  // PAGE_RANK_SOLVER_INCLUDED
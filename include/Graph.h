#ifndef GRAPH_INCLUDED
#define GRAPH_INCLUDED

#define FLOAT_PRECISION 0.00000001

#include <igraph.h>
#include <iostream>
#include <math.h>
#include <stdio.h>
#include <string>
#include <unordered_set>
#include <vector>

#include "Constants.h"

using namespace std;

// This class is used to store graph data.

class Graph {
    public:
        Graph() {}
        Graph(const Graph&) = delete;  // disable the copy constr
        // Accessories
        long int num_vertices() const;
        long int find_node_max_deg() const;
        bool is_connect(long int i, long int j) const;
        double get_d_sqrt(size_t idx) const;
        double get_dn_sqrt(size_t idx) const;
        const unordered_set<long int>* get_neighbors(long int idx) const;
        // Read graph data
        void read_graph(const string fname, const string graph_type);
        void read_graphml(string fname);
        void read_snap_txt(string fname);

    private:
        void init(igraph_t *graph);
        void init_neighbors(igraph_t *graph);
        void init_d_sqrt();
        void init_dn_sqrt();
    
    private:
        vector<unordered_set<long int> > neighbors;
        vector<double> d_sqrt;
        vector<double> dn_sqrt;
};

#endif  // GRAPH_INCLUDED
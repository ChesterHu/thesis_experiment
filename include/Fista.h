#ifndef FISTA_INCLUDED
#define FISTA_INCLUDED

#include <cmath>
#include <unordered_set>
#include <vector>

#include "Graph.h"
#include "PageRankSolver.h"

using namespace std;

// This class implements Fast Iterative Shrinkage Threshold Algorighm (FISTA).

class Fista : public PageRankSolver {
    public:
        Fista(double alpha, double epsilon, double rho, Graph& graph);
        void set_beta_constant(bool flag);

    private:
        bool is_nonzero(long int node) const;

        void update_gradient(vector<pair<long int, double>>& node_to_dy, unordered_set<long int>& nodes_touched);
        void update_nonzeros(unordered_set<long int>& nodes_touched);

        double get_dx(long int node) const;
        double get_beta();

        virtual void init_x();
        virtual void update(int num_iter);
        virtual bool is_converged(int num_iter);

    private:
        double t;
        bool is_constant_beta;
        vector<double> y;
};


#endif  // FISTA_INCLUDED
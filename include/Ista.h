#ifndef ISTA_INCLUDED
#define ISTA_INCLUDED

#include <unordered_set>
#include <vector>

#include "Graph.h"
#include "PageRankSolver.h"

using namespace std;

// This class implements Iterative Shrinkage Threshold Algorighm (ISTA).

class Ista : public PageRankSolver {
    public:
        Ista(double alpha, double epsilon, double rho, Graph& graph);

    private:
        virtual void update(int num_iter);
};

#endif  // ISTA_INCLUDED
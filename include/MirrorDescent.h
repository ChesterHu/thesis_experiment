#ifndef MIRROR_DESCENT_INCLUDED
#define MIRROR_DESCENT_INCLUDED

#include <cmath>
#include <unordered_set>
#include <vector>

#include "Graph.h"
#include "PageRankSolver.h"

using namespace std;

// This class implements accelerated proximal gradient descent.

class MirrorDescent : public PageRankSolver {
    public:
        MirrorDescent(double alpha, double epsilon, double rho, Graph& graph);
        void set_restart_flag(bool flag);

    private:
        bool is_nonzero(long int node) const;
        double get_dx(double y, double z, double prev_x, double next_tau) const;
        double get_dy(long int node) const;
        double get_dz(long int node, double gamma) const;
        double get_next_tau(int num_iter) const;
        double get_tau(int num_iter) const;
        void update_gradient(vector<pair<long int, double> >& node_to_dx, unordered_set<long int>& nodes_touched);
        void update_nonzeros(unordered_set<long int>& nodes_touched);

        virtual void init_x();
        virtual void update(int num_iter);
        virtual bool is_converged(int num_iter);
        
    private:
        bool restart_flag;
        int iter_cnt;
        vector<double> y;
        vector<double> z;
};

#endif  // MIRROR_DESCENT_INCLUDED
#ifndef TEST_INCLUDED
#define TEST_INCLUDED

#include <algorithm>
#include <iostream>
#include <vector>

#include "Appr.h"
#include "Constants.h"
#include "Fista.h"
#include "Graph.h"
#include "Ista.h"
#include "MirrorDescent.h"
#include "Objective.h"
#include "ProximalGradient.h"
#include "RandomCoordinateDescent.h"
#include "Utilities.h"

using namespace std;

//////////////////////////////
// Test functions declarations
//////////////////////////////

// Print out graph settings
void print_settings();

// perform all test
void test_all(Graph& graph);

// Evaluate function value
void test_fvalue(Graph& graph, Objective& obj);

// Evaluate gradient
void test_gradient(Graph& graph, Objective& obj);

// Evaluate proximal gradient
void test_proximal_gradient(Graph&, Objective& obj);

// Evaluate Appr class
void test_appr(double alpha, double epsilon, double rho, Graph& graph, Objective& obj);

// Test page rank solver
void test_pagerank_solver(PageRankSolver& solver, Graph& graph, bool evaluate_fvalue = true);

// Test sweep
void test_sweep(PageRankSolver& solver);

#endif
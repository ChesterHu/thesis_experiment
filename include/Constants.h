#ifndef CONSTANTS_INCLUDED
#define CONSTANTS_INCLUDED

#include <string>
#include <vector>

using namespace std;

// Graph types
const string SNAP_TXT = "txt";
const string GRAPHML  = "graphml";

// Data set path
const string DATA_PATH                = "data/";

// Output file path
const string OUTPUT_FILE_PATH         = "out/";

// Graph names
const string JOHNS_HOPKINS_GRAPH      = "JohnsHopkins";
const string PPI_MIPS_GRAPH           = "ppi_mips";
const string US_ROADS_GRAPH           = "usroads-cc";
const string SENATE_GRAPH             = "senate";
const string MAMMOTH_GRAPH            = "mammoth";
const string YOUTUBE_GRAPH            = "com-youtube.ungraph";
const string ORKUT_GRAPH              = "com-orkut.ungraph";
const string CIT_PATENTS_GRAPH        = "cit-Patents";
const string COM_LJ_GRAPH             = "com-lj.ungraph";

// Graph types
const string JOHNS_HOPKINS_GRAPH_TYPE = GRAPHML;
const string PPI_MIPS_GRAPH_TYPE      = GRAPHML;
const string US_ROADS_GRAPH_TYPE      = GRAPHML;
const string SENATE_GRAPH_TYPE        = GRAPHML;
const string MAMMOTH_GRAPH_TYPE       = GRAPHML;
const string YOUTUBE_GRAPH_TYPE       = SNAP_TXT;
const string ORKUT_GRAPH_TYPE         = SNAP_TXT;
const string CIT_PATENTS_GRAPH_TYPE   = SNAP_TXT;
const string COM_LJ_GRAPH_TYPE        = SNAP_TXT;

// Graph file full name
const string JOHNS_HOPKINS_GRAPH_NAME = DATA_PATH+JOHNS_HOPKINS_GRAPH+"."+JOHNS_HOPKINS_GRAPH_TYPE;
const string PPI_MIPS_GRAPH_NAME      = DATA_PATH+PPI_MIPS_GRAPH+"."+PPI_MIPS_GRAPH_TYPE;
const string US_ROADS_GRAPH_NAME      = DATA_PATH+US_ROADS_GRAPH+"."+US_ROADS_GRAPH_TYPE;
const string SENATE_GRAPH_NAME        = DATA_PATH+SENATE_GRAPH+"."+SENATE_GRAPH_TYPE;
const string MAMMOTH_GRAPH_NAME       = DATA_PATH+MAMMOTH_GRAPH+"."+MAMMOTH_GRAPH_TYPE;
const string YOUTUBE_GRAPH_NAME       = DATA_PATH+YOUTUBE_GRAPH+"."+YOUTUBE_GRAPH_TYPE;
const string ORKUT_GRAPH_NAME         = DATA_PATH+ORKUT_GRAPH+"."+ORKUT_GRAPH_TYPE;
const string CIT_PATENTS_GRAPH_NAME   = DATA_PATH+CIT_PATENTS_GRAPH+"."+CIT_PATENTS_GRAPH_TYPE;
const string COM_LJ_GRAPH_NAME        = DATA_PATH+COM_LJ_GRAPH+"."+COM_LJ_GRAPH_TYPE;

// Output file suffix
const string JOHNS_HOPKINS_OUTPUT_FILE_SUFFIX = "_jh";
const string PPI_MIPS_OUTPUT_FILE_SUFFIX      = "_ppi";
const string US_ROADS_OUTPUT_FILE_SUFFIX      = "_usroads";
const string SENATE_OUTPUT_FILE_SUFFIX        = "_senate";
const string MAMMOTH_OUTPUT_FILE_SUFFIX       = "_mammoth";
const string YOUTUBE_OUTPUT_FILE_SUFFIX       = "_youtube";
const string ORKUT_OUTPUT_FILE_SUFFIX         = "_orkut";
const string CIT_PATENTS_OUTPUT_FILE_SUFFIX   = "_cit";
const string COM_LJ_OUTPUT_FILE_SUFFIX        = "_comlj";

// All graphs in vector
const vector<string> GRAPHS = {
    PPI_MIPS_GRAPH,
    JOHNS_HOPKINS_GRAPH,
    US_ROADS_GRAPH,
    SENATE_GRAPH,
    MAMMOTH_GRAPH,
    YOUTUBE_GRAPH,
    ORKUT_GRAPH,
    CIT_PATENTS_GRAPH,
    COM_LJ_GRAPH
};

const vector<string> GRAPH_NAMES = {
    PPI_MIPS_GRAPH_NAME,
    JOHNS_HOPKINS_GRAPH_NAME,
    US_ROADS_GRAPH_NAME,
    SENATE_GRAPH_NAME,
    MAMMOTH_GRAPH_NAME,
    YOUTUBE_GRAPH_NAME,
    ORKUT_GRAPH_NAME,
    CIT_PATENTS_GRAPH_NAME,
    COM_LJ_GRAPH_NAME
};

const vector<string> GRAPH_TYPES = {
    PPI_MIPS_GRAPH_TYPE,
    JOHNS_HOPKINS_GRAPH_TYPE,
    US_ROADS_GRAPH_TYPE,
    SENATE_GRAPH_TYPE,
    MAMMOTH_GRAPH_TYPE,
    YOUTUBE_GRAPH_TYPE,
    ORKUT_GRAPH_TYPE,
    CIT_PATENTS_GRAPH_TYPE,
    COM_LJ_GRAPH_TYPE
};

const vector<string> OUTPUT_FILE_SUFFIXES = {
    PPI_MIPS_OUTPUT_FILE_SUFFIX,
    JOHNS_HOPKINS_OUTPUT_FILE_SUFFIX,
    US_ROADS_OUTPUT_FILE_SUFFIX,
    SENATE_OUTPUT_FILE_SUFFIX,
    MAMMOTH_OUTPUT_FILE_SUFFIX,
    YOUTUBE_OUTPUT_FILE_SUFFIX,
    ORKUT_OUTPUT_FILE_SUFFIX,
    CIT_PATENTS_OUTPUT_FILE_SUFFIX,
    COM_LJ_OUTPUT_FILE_SUFFIX
};

///////////////////
// Single constants
///////////////////
const int MAX_ITER            = 20000000;
const int NUM_TRIES_TO_AVG    = 1;   // number of tries to measure the avg time
const int NUM_POINTS_IN_PLOT  = 20;  // number of points in the report plot
const long int PRINT_FREQENCY = 50;  // the freqency of printing out progress

const double ALPHA   = 0.05;
const double EPSILON = 0.00001;
const double RHO     = 0.00001;

const double MULT_CLUSTERS_ALPHA = 0.05;
const double MULT_CLUSTERS_RHO = 0.000001;

//////////////////
// Range constants
//////////////////

const double ALPHA_START   = 0.05;
const double ALPHA_END     = 0.9;
const double RHO_START     = 0.00000001;
const double RHO_END       = 0.001;

// const double PERCENTAGE_TO_TEST   = .01;

// Algorithm names
const string ISTA                  = "ista";
const string FISTA                 = "fista";
const string FISTA_CONST           = "fista_const";
const string LINEAR_COUPLE         = "linear_couple";
const string LINEAR_COUPLE_RESTART = "linear_couple_restart";
const string RAND_COORD_DESCENT    = "rand_coord";

const vector<string> ALGORITHM_NAMES = {
    ISTA,
    FISTA,
    FISTA_CONST,
    LINEAR_COUPLE,
    LINEAR_COUPLE_RESTART,
    RAND_COORD_DESCENT
};

#endif  // CONSTANTS_INCLUDED
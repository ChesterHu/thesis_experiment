#ifndef PROXIMAL_GRADIENT_INCLUDED
#define PROXIMAL_GRADIENT_INCLUDED

#include <vector>

#include "Graph.h"
#include "Objective.h"

using namespace std;

// This class is used to compare the result of different methods. It's developed 
// mainly for correctness, so the time/space complexity is not optimized.

class ProximalGradient {
    public:
        ProximalGradient(Graph& graph, Objective& obj);
        vector<double> minimize(vector<double>& x, int max_iter) const;

    private:
        Graph graph;
        Objective obj;

    private:
        void proximal_step(vector<double>& x, vector<double>& gradient) const;
};

#endif  // PROXIMAL_GRADIENT_INCLUDED
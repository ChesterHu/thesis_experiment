#ifndef UTILITIES_INCLUDED
#define UTILITIES_INCLUDED

#include <algorithm>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <unordered_set>

#include "Constants.h"
#include "Fista.h"
#include "MirrorDescent.h"
#include "Ista.h"
#include "RandomCoordinateDescent.h"
#include "PageRankSolver.h"

using namespace std;
/////////////////////////////////
// Utilities used for experiments
/////////////////////////////////

bool get_y_or_n();

vector<double> linspace(double start, double end, int num_pts = 20);

vector<double> logspace(double start, double end, int num_pts = 20);

double get_avg(vector<double>& nums);

double get_std(vector<double>& nums);

void run_experiment(int graph_id, Graph& graph);

void find_clusters(int graph_id, Graph& graph);

PageRankSolver* select_algorithm(Graph& graph, int& alg_id);

void run_alpha_experiment(int graph_id, Graph& graph);

void run_rho_experiment(int graph_id, Graph& graph);

void run_nonzeros_experiment(int graph_id, Graph& graph);

void run_multiple_clusters_experiment(int graph_id, Graph& graph);

long int get_stepsize(long int total, double percentage);

void alpha_experiment(Graph& graph, string fname, PageRankSolver& solver);

void rho_experiment(Graph& graph, string fname, PageRankSolver& solver);

void nonzeros_experiment(long int seed_node, string fname, PageRankSolver& solver);

void multiple_clusters_experiment(Graph& graph, string fname, PageRankSolver& solver);

#endif  // UTILITIES_INCLUDED
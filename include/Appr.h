#ifndef APPR_INCLUDED
#define APPR_INCLUDED

#include <queue>
#include <unordered_set>
#include <vector>

#include "Graph.h"
#include "PageRankSolver.h"

using namespace std;

// This class implements Approximate Personalized Page Rank (APPR) algorithm.

class Appr : public PageRankSolver {
    public:
        Appr(double alpha, double epsilon, double rho, Graph& graph);

    private:
        bool is_nonzero(long int node) const;
        virtual void update(int num_iter);
};

#endif  // APPR_INCLUDED
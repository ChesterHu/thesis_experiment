#ifndef RANDOM_COORDINATE_DESCENT_INCLUDED
#define RANDOM_COORDINATE_DESCENT_INCLUDED

#include <cmath>
#include <vector>

#include "Graph.h"
#include "PageRankSolver.h"

using namespace std;

// This class implements random coordinate descent.

class RandomCoordinateDescent: public PageRankSolver {
    public:
        RandomCoordinateDescent(double alpha, double epsilon, double rho, Graph& graph);

    private:
        long int sample();
        virtual void update(int num_iter);
        virtual void init_x();
        virtual bool is_converged(int num_iter);

    private:
        vector<long int> candidate_nodes;
};

#endif  // RANDOM_COORDINATE_DESCENT included
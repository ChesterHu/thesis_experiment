# Optimization algorithms C++ Experiments

## Introduction
This repository consists following optimization algorithms to solve Approximate Personalized PageRank (APPR) problem.
* ISTA
* FISTA
* Mirror descent
* Randomized coordinate descent (todo)

## Prerequisite
1. g++
2. igraph library
3. python3 (for plotting)
# Compiler
CC=g++

# Directories
EXC_DIR=bin
INC_DIR=include
OBJ_DIR=obj
SRC_DIR=src

# Compiler flags
CFLAGS=\
	-O3\
	-ligraph -std=c++11\
	-I/usr/local/include/igraph\

# Files
_DEP=\
	Appr.h\
	Fista.h\
	Graph.h\
	Ista.h\
	MirrorDescent.h\
	Objective.h\
	PageRankSolver.h\
	ProximalGradient.h\
	RandomCoordinateDescent.h\
	Test.h\
	Utilities.h

_SRC=main.cpp $(_DEP:.h=.cpp)
_OBJ=$(_SRC:.cpp=.o)
_EXC=exe

# Files with relative path
DEP=$(patsubst %,$(INC_DIR)/%,$(_DEP))
SRC=$(patsubst %,$(SRC_DIR)/%,$(_SRC))
OBJ=$(patsubst %,$(OBJ_DIR)/%,$(_OBJ))
EXC=$(patsubst %,$(EXC_DIR)/%,$(_EXC))

# Compile
all: $(EXC) $(SRC)

$(EXC): $(OBJ)
	$(CC) $^ -o $@ $(CFLAGS)

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.cpp $(DEP)
	$(CC) -c $< -o $@ $(CFLAGS)

.PHONY: clean

clean:
	rm -f $(EXC) $(OBJ)